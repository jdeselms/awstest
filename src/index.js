var createResponse = (code, body) => {
	return {
		statusCode: code,
		body: JSON.stringify(body),
		headers: {}
	};
}

exports.handler = (event, context, callback) => {
	var result = {
		k1: event.queryStringParameters.key1,
		k2: event.queryStringParameters.key2,
		k3: event.queryStringParameters.key3
	};

	callback(null, createResponse(200, result));
};